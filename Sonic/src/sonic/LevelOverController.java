package sonic;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.NiftyEventSubscriber;
import de.lessvoid.nifty.elements.events.NiftyMousePrimaryClickedEvent;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.screen.ScreenController;

public class LevelOverController implements ScreenController {

	public void bind(Nifty nifty, Screen screen){ } 
	public void onStartScreen(){ } 
	public void onEndScreen(){ } 

	@NiftyEventSubscriber(pattern="btn-levelOver-.*")
	public void onClick(String id, NiftyMousePrimaryClickedEvent event ){
		if(id.equals("btn-levelOver-continue")) {					// Next level
			GameState.respawn();
			Main.game.enterState(Main.gameState.GAMESTATE.value);	
		} else if(id.equals("btn-levelOver-restart")) {				// Restart Level
			GameState.currentLevel--;
			GameState.respawn();
			Main.game.enterState(Main.gameState.GAMESTATE.value);	
		} else if(id.equals("btn-levelOver-menu")) {				// Main Menu
			Main.game.enterState(Main.gameState.MAINMENU.value);	
		} else if(id.equals("btn-levelOver-exit")) {				// Exit
			System.exit(0);											
		}
	}
}
