package sonic;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;

import de.lessvoid.nifty.slick2d.NiftyStateBasedGame;

public class Main extends NiftyStateBasedGame {

	static Main game;
	
	public static int screenWidth = 800, screenHeight = 640;	 // Window dimensions
	public static final String gamename = "Rift Keeper";

	// Image names/ paths
	public static final String BLANK_IMG = "sonic/res/blank.png";	
	
	public static final String PLAYER_IMG = "sonic/res/block.png";	
	public static final String PLAYER_ATK_ANIM = "sonic/res/blockAttack.png";
	public static final String PLAYER_ATK_BLANK = "sonic/res/blankBlockAttack.png";// Blank image for bounding boxes
	
	public static final String ENEMY_IMG = "sonic/res/enemy.png";	// Blank image for bounding boxes
	public static final String COLLECTIBLE_IMG = "sonic/res/brain.png";	
	public static final String END_IMG = "sonic/res/end.png";	
	
	// http://opengameart.org/sites/default/files/1_generic_comic_book.png
	public static final String BACKGROUND_IMG = "sonic/res/background.png";
	// http://opengameart.org/content/ghost-reaper
	public static final String GHOST_ANIM = "sonic/res/ghost.png";
	// http://opengameart.org/content/skeleton
	public static final String SKELLY_ANIM = "sonic/res/skeleton.png";
	
	// Tiles made by me
	
	public static enum gameState {
		MAINMENU(0), GAMESTATE(1), OPTIONS(2), CREDITS(3), GAMEOVER(4), LEVELOVER(5), CONTROLS(6);
		int value;

		private gameState(int value) { // Because they are expected to be ints when being passed in
			this.value = value;
		}
	};
	
	public Main(String gamename, int width, int height) {
		super(gamename);
		screenWidth = width;
		screenHeight = height;
	}
	
	public void initStatesList(GameContainer gc) throws SlickException {
		this.addState(new MainMenu(gameState.MAINMENU.value));
		this.addState(new GameState(gameState.GAMESTATE.value));
		this.addState(new Credits(gameState.CREDITS.value));
//		this.addState(new Options(gameState.OPTIONS.value));
		this.addState(new GameOver(gameState.GAMEOVER.value));
		this.addState(new LevelOver(gameState.LEVELOVER.value));
		this.addState(new Controls(gameState.CONTROLS.value));

		this.enterState(gameState.MAINMENU.value);
	}

	public static void main(String[] args) {
		AppGameContainer appGC;
		game = new Main(gamename, 800, 640);
		try {
			appGC = new AppGameContainer(game);
			appGC.setDisplayMode(screenWidth, screenHeight, false);
			appGC.setVSync(true);
			appGC.start();
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}
}
