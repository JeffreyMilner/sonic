package sonic;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.slick2d.NiftyOverlayBasicGameState;

public class Controls extends NiftyOverlayBasicGameState{

	public Controls(int state) { }

	public void initGameAndGUI(GameContainer container, StateBasedGame game) throws SlickException {
		initNifty(container, game);
	}
	
	public void renderGame(GameContainer container, StateBasedGame game, Graphics g) throws SlickException { }

	public int getID() {
		return Main.gameState.CONTROLS.value;
	}

	protected void prepareNifty(Nifty nifty, StateBasedGame game) {
		ControlController CC = new ControlController();
		nifty.fromXml("niftyRes/controls.xml", "controls", CC);
	}

	protected void enterState(GameContainer container, StateBasedGame game) throws SlickException { }
	protected void leaveState(GameContainer container, StateBasedGame game) throws SlickException { }
	protected void updateGame(GameContainer container, StateBasedGame game, int delta) throws SlickException { }
}
