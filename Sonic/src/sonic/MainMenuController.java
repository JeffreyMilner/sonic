package sonic;

import org.newdawn.slick.gui.ComponentListener;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.NiftyEventSubscriber;
import de.lessvoid.nifty.elements.events.NiftyMousePrimaryClickedEvent;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.screen.ScreenController;

public class MainMenuController implements ScreenController {

	private ComponentListener startGameListener, optionsListener, creditsListener, exitListener, controlsListener;
	
	public void bind(Nifty nifty, Screen screen){
	}

	public void onStartScreen(){
	}

	public void onEndScreen(){
	}

	@NiftyEventSubscriber(pattern="btn-mainMenu-.*")
	public void onClick(String id, NiftyMousePrimaryClickedEvent event ){
		if(id.equals("btn-mainMenu-start")) {
			if(startGameListener != null) {
				startGameListener.componentActivated(null);
			}
		} else if(id.equals("btn-mainMenu-controls")) {
			if(controlsListener != null) {
				controlsListener.componentActivated(null);
			}
		} else if(id.equals("btn-mainMenu-credit")) {
			if(creditsListener != null) {
				creditsListener.componentActivated(null);
			}
		} else if(id.equals("btn-mainMenu-exit")) {
			if(exitListener != null) {
				exitListener.componentActivated(null);
			}
		}
	}
	
	public void onStartPress(ComponentListener press) {
		startGameListener = press;
	}
	public void onOptionsPress(ComponentListener press) {
		optionsListener = press;
	}
	public void onControlsPress(ComponentListener press) {
		controlsListener = press;
	}
	public void onCreditsPress(ComponentListener press) {
		creditsListener = press;
	}
	public void onExitPress(ComponentListener press) {
		exitListener = press;
	}
}
