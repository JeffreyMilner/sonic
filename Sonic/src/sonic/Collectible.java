package sonic;

import sonic.Main;
import jig.Entity;
import jig.ResourceManager;


/**
 * @author Jeff
 * 
 * This will be for the brains etc that will be scattered around the map for the player
 * to pick up, and maybe also for drops when something is defeated
 */
public class Collectible extends Entity {
	
	public final float origX;

	public Collectible(int x, int y) {
		super(x, y);
		origX = x;
		addImageWithBoundingBox(ResourceManager.getImage(Main.COLLECTIBLE_IMG));		
	}
	
	public void update() {
		setX(origX + GameState.xOffset);
	}
}
