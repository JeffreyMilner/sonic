package sonic;

import org.newdawn.slick.Animation;
import org.newdawn.slick.Color;

import jig.Entity;
import jig.ResourceManager;

public class Player extends Entity {

	private int jumping = 0;
	private float yVelocity = 0, xVelocity = 0;
	private boolean falling;
	private boolean standing;
	public boolean jumpPressed = false;
	public boolean attacking = false;
	public boolean hasDoubleJumped = false;
	private int gravity = 3;
	public int attackTimer = 80;
	public Animation attackAnim;
	
	public Player(final float x, final float y) {
		super(x, y);
		addImageWithBoundingBox(ResourceManager.getImage(Main.PLAYER_IMG));
		attackAnim = new Animation(ResourceManager.getSpriteSheet(Main.PLAYER_ATK_ANIM, 45, 45), 0, 0, 1, 0, true, 100, true);
	}
	
	public void move(float dx, float dy) {
		setXVelocity(dx);
	}
	
	public void attack() {
		attacking = true;
		this.removeImage(ResourceManager.getImage(Main.PLAYER_IMG));
		this.addImageWithBoundingBox(ResourceManager.getImage(Main.PLAYER_ATK_BLANK));
		addAnimation(attackAnim);
	}
	
	public void endAttacking() {
		if(attacking) {
			attacking = false;
			this.removeImage(ResourceManager.getImage(Main.PLAYER_ATK_ANIM));
			this.removeAnimation(attackAnim);
			this.addImage(ResourceManager.getImage(Main.PLAYER_IMG));
		}
	}
	
	public void die() {
		GameState.lives--;
		if(GameState.lives >= 1) {
			GameState.respawn();
		} else {
			GameState.gameOver(Color.red);
		}
	}
	
	public void jump(int delta) {
		if( !hasDoubleJumped ) {
			setStanding(false);
			if(isJumping() == 1) {
				if(isJumping() < 0) {
					addYVelocity(-60);
				} else {
					setYVelocity(-60);
				}				
				hasDoubleJumped = true;
			} else {
				attack();
				setYVelocity(-60);
				setJumping(1);
			}
			
		}
	}
	
	public void gravityCheck(int delta) {
		if(isJumping() == 0 && !isBlockedDown(delta)) { // If player is not on a tile, apply gravity
			setFalling(true);
		}
        if (isJumping() > 0) {
            yVelocity += gravity;
            if (getYVelocity() > 0) {
                setFalling(true);
                setStanding(false);
            }
        }
        if (isFalling()) {
        	setStanding(false);
            yVelocity += gravity;
            if (isBlockedDown(delta/4)) {
            	endAttacking();
            	setFalling(false);
                setStanding(true);
                setYVelocity(0);
            } 
        }
        if(isStanding()) {
        	setFalling(false);
        	setJumping(0);
        	hasDoubleJumped = false;
        	setY(((int)getY() / GameState.tileSize) * 32 + 16);
        	if(getCoarseGrainedMaxY() > GameState.tileSize * GameState.levelMap.getHeight() - GameState.tileSize) {
        		setY((GameState.tileSize * GameState.levelMap.getHeight()) - GameState.tileSize*1.5f);
        	}
        }
    }

	public void update(final int delta) {
		if((yVelocity > 0) || (yVelocity < 0)){
			setY(getY() + yVelocity * delta*0.01f);
		} else {
			setYVelocity(0);
		}
		if(xVelocity > 0 || xVelocity < 0) {
			setX(getX() + xVelocity * delta*0.01f);
		}
		
		if(getCoarseGrainedMinX() < 0 || isBlockedLeft(1)) {
			setX(getX() + 3);
		}
		
		if(isBlockedUp(delta)) { 	
			setYVelocity(0);
		}
		
		gravityCheck(delta);
	}
	
	static int isBlocked(float x, float y) {
        int xBlock = (int)x / GameState.tileSize;
        if(xBlock > GameState.levelMap.getWidth()-1) {
        	xBlock = GameState.levelMap.getWidth()-1;
        } else if(xBlock < 0) {
        	xBlock = 0;
        }
        
        int yBlock = (int)y / GameState.tileSize;
        if(yBlock > GameState.levelMap.getHeight()-1) {
        	yBlock = GameState.levelMap.getHeight()-1;
        } else if(yBlock < 0) {
        	yBlock = 0;
        }
   
        return GameState.blockedLayer[xBlock][yBlock];
    }
	
	boolean isBlockedDown(float fdelta) {	// Can change these to work differently depending on what is returned fron the block check
		if((isBlocked(getCoarseGrainedMinX() - GameState.xOffset, getCoarseGrainedMaxY() + fdelta) != GameState.blocks.blank.value 
				|| isBlocked(getCoarseGrainedMaxX() - GameState.xOffset, getCoarseGrainedMaxY() + fdelta) != GameState.blocks.blank.value)) {
			return true;				
		}
		return false;
	}

	boolean isBlockedUp(float fdelta) { 	// Only block the upward direction if it is a brick wall
		if((isBlocked(getCoarseGrainedMinX() - GameState.xOffset, getCoarseGrainedMinY() - fdelta) == GameState.blocks.brickWall.value // The bricks
				|| isBlocked(getCoarseGrainedMaxX() - GameState.xOffset, getCoarseGrainedMinY() - fdelta) == GameState.blocks.brickWall.value)) {
			return true;
		} 
		return false;
	}

	// And the same for the sideways directions
	boolean isBlockedRight(float fdelta) {
		if((isBlocked(getCoarseGrainedMaxX() + fdelta - GameState.xOffset, getCoarseGrainedMinY()) == GameState.blocks.brickWall.value 
				|| isBlocked(getCoarseGrainedMaxX() + fdelta - GameState.xOffset, getCoarseGrainedMaxY()) == GameState.blocks.brickWall.value)) {
			return true;
		}
		return false;
	}

	boolean isBlockedLeft(float fdelta) {
		if((isBlocked(getCoarseGrainedMinX() - fdelta - GameState.xOffset, getCoarseGrainedMinY()) == GameState.blocks.brickWall.value
				|| isBlocked(getCoarseGrainedMinX() - fdelta - GameState.xOffset, getCoarseGrainedMaxY()) == GameState.blocks.brickWall.value)) {
			return true;
		}
		return false;
	}
	

	public float getYVelocity() { return yVelocity; }
	public void setYVelocity(float yVelocity) { this.yVelocity = yVelocity; }
	public void addYVelocity(float yV) { this.yVelocity += yV; }
	public void subtractYVelocity(float yV) { this.yVelocity -= yV; }
	public float getXVelocity() { return xVelocity; }
	public void setXVelocity(float xVelocity) { this.xVelocity = xVelocity; }
	public boolean isStanding() { return standing; }
	public void setStanding(boolean standing) { this.standing = standing; }
	public boolean isFalling() { return falling; }
	public void setFalling(boolean falling) { this.falling = falling; }
	public int isJumping() { return jumping; }
	public void setJumping(int jumping) { this.jumping = jumping; }
	public boolean isJumpPressed() { return jumpPressed; }
	public void setJumpPressed(boolean jumpPressed) { this.jumpPressed = jumpPressed; }
}
