package sonic;

import java.awt.Font;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.state.StateBasedGame;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.slick2d.NiftyOverlayGameState;


public class GameOver extends NiftyOverlayGameState {

	TrueTypeFont font;
	static boolean win = false;

	public GameOver(int state) {

	}

	protected void initGameAndGUI(GameContainer container, StateBasedGame game) throws SlickException {
		initNifty(container, game);

		Font awtFont4 = new Font("Calibre", Font.BOLD, 20);
		font = new TrueTypeFont(awtFont4, false);
	}

	public int middleOf(String text) {
		return Main.screenWidth/2 - font.getWidth(text)/2;
	}

	protected void enterState(GameContainer container, StateBasedGame game) throws SlickException { }
	protected void leaveState(GameContainer container, StateBasedGame game) throws SlickException { }
	protected void renderGame(GameContainer container, StateBasedGame game, Graphics g) throws SlickException {
		int ix = 2;
		
		String Lose1 = "You have not survived, you can try again to do better, or accept defeat";
		String Lose2 = "You killed " + GameState.killCount + " enemies and collected " + GameState.grabCount + " items during your attempt";

		String Win1 = "Congratulations!";
		String Win2 = "You have beat the game.";
		
		if(!win) {
			font.drawString(middleOf(Lose1), ix++ * 30, Lose1, Color.red);
			font.drawString(middleOf(Lose2), ix++ * 30, Lose2, Color.red);
		} else {
			font.drawString(middleOf(Win1), ix++ * 30, Win1, Color.green);
			font.drawString(middleOf(Win2), ix++ * 30, Win2, Color.green);
		}

	}
	protected void updateGame(GameContainer container, StateBasedGame game, int delta) throws SlickException { }

	protected void prepareNifty(Nifty nifty, StateBasedGame game) { 
		GameOverController GOC = new GameOverController();

		nifty.fromXml("niftyRes/gameOver.xml", "gameOver", GOC);
	}

	public int getID() {
		return Main.gameState.GAMEOVER.value;
	}

	public void mouseWheelMoved(int change) { }
	public void mouseClicked(int button, int x, int y, int clickCount) { }
	public void mousePressed(int button, int x, int y) { }
	public void mouseReleased(int button, int x, int y) { }
	public void mouseMoved(int oldx, int oldy, int newx, int newy) { }
	public void mouseDragged(int oldx, int oldy, int newx, int newy) { }
	public void setInput(Input input) { }
	public boolean isAcceptingInput() { return false; }
	public void inputEnded() { }
	public void inputStarted() { }
	public void keyPressed(int key, char c) { }
	public void keyReleased(int key, char c) { }
	public void controllerLeftPressed(int controller) { }
	public void controllerLeftReleased(int controller) { }
	public void controllerRightPressed(int controller) { }
	public void controllerRightReleased(int controller) { }
	public void controllerUpPressed(int controller) { }
	public void controllerUpReleased(int controller) { }
	public void controllerDownPressed(int controller) { }
	public void controllerDownReleased(int controller) { }
	public void controllerButtonPressed(int controller, int button) { }
	public void controllerButtonReleased(int controller, int button) { }
}
