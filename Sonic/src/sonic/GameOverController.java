package sonic;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.NiftyEventSubscriber;
import de.lessvoid.nifty.elements.events.NiftyMousePrimaryClickedEvent;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.screen.ScreenController;

public class GameOverController implements ScreenController {

	public void bind(Nifty nifty, Screen screen){ } 
	public void onStartScreen(){ } 
	public void onEndScreen(){ } 
	
	@NiftyEventSubscriber(pattern="btn-gameOver-.*")
	public void onClick(String id, NiftyMousePrimaryClickedEvent event ){
		if(id.equals("btn-gameOver-restart")) {				// Restart level
			GameState.resetGame();
			Main.game.enterState(Main.gameState.GAMESTATE.value);
		} else if(id.equals("btn-gameOver-menu")) {			// Go to main menu
			Main.game.enterState(Main.gameState.MAINMENU.value);
		} else if(id.equals("btn-gameOver-exit")) {			// Exit the game
			System.exit(0);
		}
	}
}
