package sonic;

import java.awt.Font;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.state.StateBasedGame;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.slick2d.NiftyOverlayBasicGameState;

public class Credits extends NiftyOverlayBasicGameState{

	TrueTypeFont font;

	String designer 	= "Designer: Me";
	String programmer 	= "Programmer: Me";
	String tiles 		= "Tiles by: Me";
	String player 		= "Player by: Me";
	String controls 	= "Control images modified from: http://opengameart.org/content/a-few-gui-elements";
	String skeleton 	= "Skeleton from: \"http://opengameart.org/content/skeleton\"";
	String ghost 		= "Ghost from: \"http://opengameart.org/content/ghost-reaper\"";
	String background 	= "Background modified from: \"http://opengameart.org/sites/default/files/1_generic_comic_book.png\"";
	String idea 		= "Idea from: \"Sonic the Hedgehog\" and \"Mario\"";
	
	public Credits(int state) {
		
	}

	public void initGameAndGUI(GameContainer container, StateBasedGame game) throws SlickException {
		initNifty(container, game);
		Font awtFont4 = new Font("Calibre", Font.PLAIN, 16);
		font = new TrueTypeFont(awtFont4, false);
	}
	
	public int middleOf(String text) {
		return Main.screenWidth/2 - font.getWidth(text)/2;
	}

	public void renderGame(GameContainer container, StateBasedGame game, Graphics g) throws SlickException {
		int ix = 1; // Makes it easier to position the strings
		font.drawString(middleOf(designer), 	ix++ * 50, designer, 	Color.green);
		font.drawString(middleOf(programmer), 	ix++ * 50, programmer, 	Color.green);
		font.drawString(middleOf(tiles), 		ix++ * 50, tiles, 		Color.green);
		font.drawString(middleOf(player), 		ix++ * 50, player, 		Color.green);
		font.drawString(middleOf(controls),		ix++ * 50, controls, 	Color.green);
		font.drawString(middleOf(skeleton), 	ix++ * 50, skeleton, 	Color.green);
		font.drawString(middleOf(ghost), 		ix++ * 50, ghost, 		Color.green);
		font.drawString(middleOf(background), 	ix++ * 50, background,	Color.green);
		font.drawString(middleOf(idea), 		ix++ * 50, idea, 		Color.green);
	}

	public int getID() {
		return Main.gameState.CREDITS.value;
	}

	protected void prepareNifty(Nifty nifty, StateBasedGame game) {
		CreditController CC = new CreditController();
		nifty.fromXml("niftyRes/credits.xml", "credits", CC);
	}

	protected void enterState(GameContainer container, StateBasedGame game) throws SlickException { }
	protected void leaveState(GameContainer container, StateBasedGame game) throws SlickException { }
	protected void updateGame(GameContainer container, StateBasedGame game, int delta) throws SlickException { }
}
