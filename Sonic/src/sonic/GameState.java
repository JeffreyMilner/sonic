package sonic;

import java.util.ArrayList;
import java.util.Iterator;

import jig.Entity;
import jig.ResourceManager;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.state.transition.FadeInTransition;
import org.newdawn.slick.state.transition.FadeOutTransition;
import org.newdawn.slick.tiled.TiledMap;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.slick2d.NiftyOverlayBasicGameState;

public class GameState extends NiftyOverlayBasicGameState	 {

	static int tileSize = 32; 	// The pixel width/ height, still a square

	public static TiledMap levelMap;

	public static int xOffset = 0;

	public static boolean debug = false;
	public static boolean paused = false;

	// This will keep a list of Tiles that are blocked
	public static int blockedLayer[][];
	public static int spawnLayer[][];
	public static int backgroundLayer[][];

	static Player player;

	static ArrayList<Collectible> collectibles;
	static ArrayList<Ghost> ghosts;
	static ArrayList<Skeleton> skeletons;
	static ArrayList<EndPoint> endFlags;

	static PauseController PC;

	public static int points = 0;
	public static int lives = 3;

	public static int enemyTotal = 0, itemTotal = 0;
	public static int grabCount = 0, killCount = 0;

	public static int levelCount = 3;
	public static int currentLevel = 1;

	public static enum layer {
		collision(0), spawn(0), background(0);
		int value;

		private layer(int value) { // Because they are expected to be ints when being passed in
			this.value = value;
		}

		private void getLayerNum(String name) {
			this.value = levelMap.getLayerIndex(name);
		}
	};

	public static enum blocks {
		blank(0), leftCornerDirt(1), dirt(2), rightCornerDirt(3), leftCornerGrass(4), 
		topGrass(5),  rightCornerGrass(6), brickWall(7), collectibleSpawn(8), ghostSpawn(9), playerSpawn(12), 
		skellySpawn(13), rockSlab(14), endSpawn(15);
		int value;

		private blocks(int value) {
			this.value = value;
		}
	};


	public GameState(int state) {
		Entity.setCoarseGrainedCollisionBoundary(Entity.AABB);
	}

	public void initGameAndGUI(GameContainer container, StateBasedGame game) throws SlickException {
		initNifty(container, game);
		ResourceManager.loadImage(Main.BACKGROUND_IMG);
		ResourceManager.loadImage(Main.COLLECTIBLE_IMG);
		ResourceManager.loadImage(Main.PLAYER_IMG);
		ResourceManager.loadImage(Main.ENEMY_IMG);
		ResourceManager.loadImage(Main.GHOST_ANIM);
		ResourceManager.loadImage(Main.SKELLY_ANIM);
		ResourceManager.loadImage(Main.PLAYER_ATK_ANIM);
		ResourceManager.loadImage(Main.PLAYER_ATK_BLANK);
		ResourceManager.loadImage(Main.END_IMG);

		collectibles = new ArrayList<Collectible>(50);
		ghosts = new ArrayList<Ghost>(50);
		skeletons = new ArrayList<Skeleton>(50);
		endFlags = new ArrayList<EndPoint>(5);

		player = new Player(600, 400);

		//		try {
		//			levelMap = new TiledMap("levels/Level_" + currentLevel + ".tmx", true);
		//		} catch(SlickException e) {
		//			e.printStackTrace();
		//		}
		//		blocked = new int[levelMap.getWidth()][levelMap.getHeight()];
		//		spawn = new int[levelMap.getWidth()][levelMap.getHeight()];
		//		flag = new int[levelMap.getWidth()][levelMap.getHeight()];
		//
		//		// Getting the laayer indexes so that I can change their order in the .tmx file, without having to worry about it
		//		layer.collision.getLayerNum("collision");
		//		layer.background.getLayerNum("background");
		//		layer.spawn.getLayerNum("spawn");

		spawnEntities();
	}

	public static void spawnEntities() {
		ghosts.clear();
		skeletons.clear();
		collectibles.clear();
		endFlags.clear();

		try {
			levelMap = new TiledMap("levels/Level_" + currentLevel + ".tmx", true);
		} catch(SlickException e) {
			e.printStackTrace();
		}
		blockedLayer = new int[levelMap.getWidth()][levelMap.getHeight()];
		spawnLayer = new int[levelMap.getWidth()][levelMap.getHeight()];
		backgroundLayer = new int[levelMap.getWidth()][levelMap.getHeight()];

		// Getting the laayer indexes so that I can change their order in the .tmx file, without having to worry about it
		layer.collision.getLayerNum("collision");
		layer.background.getLayerNum("background");
		layer.spawn.getLayerNum("spawn");

		try {
			levelMap = new TiledMap("levels/Level_" + currentLevel + ".tmx", true);
		} catch(SlickException e) {
			e.printStackTrace();
		}

		for(int x = 0; x < levelMap.getWidth(); x++) { 	// Put it into a 2d array so that I can run through them when checking for collisins
			for(int y = 0; y < levelMap.getHeight(); y++) {
				blockedLayer[x][y] 	= levelMap.getTileId(x, y, layer.collision.value);
				spawnLayer[x][y] 	= levelMap.getTileId(x, y, layer.spawn.value);
				backgroundLayer[x][y] 		= levelMap.getTileId(x, y, layer.background.value);

				if(spawnLayer[x][y] == blocks.playerSpawn.value) {							// Setting the player's spawnpoint
					player.setPosition(x*32, y*32);
					player.setFalling(true);
				} else if(spawnLayer[x][y] == blocks.collectibleSpawn.value) {				// Setting the drop locations
					collectibles.add(new Collectible(x*32 + 16, y*32+16));
					itemTotal++;
				} else if(spawnLayer[x][y] == blocks.ghostSpawn.value) {					// Setting where the ghosts spawn
					ghosts.add(new Ghost(x*32, y*32 + 16, 2)); 								// (xPos, yPos, #ofBlocksToMove)
					enemyTotal++;
				} else if(spawnLayer[x][y] == blocks.skellySpawn.value) {					// Setting where the skeletons spawn
					skeletons.add(new Skeleton(x*32, y*32 + 16, 4)); 		
					enemyTotal++;
				} else if(spawnLayer[x][y] == blocks.endSpawn.value) {						// Setting where the skeletons spawn
					endFlags.add(new EndPoint(x*32, y*32 + 48));
				}
			}
		}
	}

	public static void resetGame() {
		currentLevel = 1;
		lives = 3;
		respawn();
	}

	public static void respawn() {
		points = 0;
		xOffset = 0;
		enemyTotal = 0;
		itemTotal = 0;
		grabCount = 0;
		killCount = 0;
		spawnEntities();
	}

	public void levelUp() {
		currentLevel++;
		Main.game.enterState(Main.gameState.LEVELOVER.value);
	}

	public static void gameOver(Color color) {
		Main.game.enterState(Main.gameState.GAMEOVER.value, new FadeOutTransition(color, 1000), new FadeInTransition(color, 1000));
	}

	public void renderGame(GameContainer container, StateBasedGame game, Graphics g) throws SlickException {
		g.drawImage(ResourceManager.getImage(Main.BACKGROUND_IMG), 0, 0);
		g.setColor(Color.white);
		// (xPos, yPos, layer#)
		levelMap.render(0 + xOffset, 0, layer.background.value);
		levelMap.render(0 + xOffset, 0, layer.collision.value);

		player.render(g);

		for(Collectible grab : collectibles){ grab.render(g);  	}
		for(Ghost enemy 	 : ghosts) 		{ enemy.render(g); 	}
		for(Skeleton enemy 	 : skeletons) 	{ enemy.render(g); 	}
		for(EndPoint end 	 : endFlags) 	{ end.render(g); 	}

		g.drawString("Score: " + points, 10, 35);
		g.drawString("Lives: " + lives,  10, 60);

		if(debug) {
			int ix = 60;
			g.drawString("Standing: " 	 + player.isStanding(), 	10, ix+=25);
			g.drawString("Jumping: " 	 + player.isJumping(), 		10, ix+=25);
			g.drawString("Falling: " 	 + player.isFalling(), 		10, ix+=25);
			g.drawString("JumpPressed: " + player.isJumpPressed(), 	10, ix+=25);		
			g.drawString("yVelocity: " 	 + player.getYVelocity(), 	10, ix+=25);
			g.drawString("xOffset: " 	 + xOffset,				 	10, ix+=25);
			g.drawString("pickupCount: " + collectibles.size(),	 	10, ix+=25);
			g.drawString("enemyCount: "  + (ghosts.size() + skeletons.size()),		 	10, ix+=25);
			g.drawString("On block: " 	 + getBlockID(player.getX(), player.getCoarseGrainedMaxY(), layer.collision.value), 10, ix+=25);			
			g.drawString("Attacking: " + player.attacking,	 	10, ix+=25);
			levelMap.render(0 + xOffset, 0, layer.spawn.value);
		}
	}

	public void updateGame(GameContainer container, StateBasedGame game, int delta) {
		Input input = container.getInput();

		float fdelta = delta*0.15f;
		float fSpeed = 9;
		if(!paused) {
			//			if(!OptionsController.wasd) {
			if (input.isKeyDown(Input.KEY_LEFT)) {
				if (!player.isBlockedLeft(fdelta)) {
					if(xOffset < 0  && player.getX() < Main.screenWidth/2 - 5) {
						xOffset += fdelta*fSpeed* delta * 0.01;
						player.setXVelocity(0);
					} else {
						player.move(-fdelta * fSpeed, 0);
					}
				} else {
					player.move(0, 0);
				}
			} else if (input.isKeyDown(Input.KEY_RIGHT)) {
				if (!player.isBlockedRight(fdelta)) {
					if(xOffset/32 > -levelMap.getWidth()+25 && player.getX() > Main.screenWidth/2 + 5) {
						xOffset -= fdelta*fSpeed* delta * 0.01;	
						player.setXVelocity(0);
					} else {
						if(xOffset/32 < -levelMap.getWidth()) {
							xOffset = -levelMap.getWidth()*32;
						}
						player.move(fdelta * fSpeed, 0);
					}
				} else {
					player.move(0, 0);
				}
			} else {
				player.move(0, 0);
			} 

			if(xOffset >= 0) {
				xOffset = 0;
			}

			if(input.isKeyPressed(Input.KEY_SPACE)) {
				player.jump(delta);
			} 
		}
		if(input.isKeyPressed(Input.KEY_V)) { 	// To toggle the debug output
			if(debug) {
				debug = false;
				Entity.setDebug(false);
			} else {
				debug = true;
				Entity.setDebug(true);			
			}
		} 
		
		if(input.isKeyPressed(Input.KEY_ADD)) {
			currentLevel++;
			respawn();
		}
		if(input.isKeyPressed(Input.KEY_SUBTRACT)) {
			currentLevel--;
			respawn();
		}

		if(input.isKeyPressed(Input.KEY_ESCAPE)) {
			if(!paused) {
				pause();
				PC.showPauseMenu();
			} else {
				PC.exitPauseMenu();
				pause();
			}
		}

		if(input.isMousePressed(1)) { 	// To check ID's of blocks by clicking on them.
			System.out.println("CollisionBlockID: "  + getBlockID(input.getMouseX(), input.getMouseY(), layer.collision.value));
			System.out.println("SpawnBlockID: " 	 + getBlockID(input.getMouseX(), input.getMouseY(), layer.spawn.value));
			System.out.println("BackgroundBlockID: " + getBlockID(input.getMouseX(), input.getMouseY(), layer.background.value));
		}
		if(!paused) {
			// ### Collisions
			for (Iterator<Collectible> i = collectibles.iterator(); i.hasNext();) {
				if(player.collides(i.next()) != null) {
					i.remove();
					points += 100;
					grabCount++;
				} 
			}

			// change to if not attacking and not facing the enemy, kill the player, else i.remove
			for (Iterator<Ghost> ghost = ghosts.iterator(); ghost.hasNext();) {
				if(player.collides(ghost.next()) != null) {
					if(player.attacking) {
						ghost.remove();
						points += 500; 
						killCount++;
					} else {
						player.die();
						break;	// So that I do not crash it with a concurrent modification exception
					} 
				}
			}
			for (Iterator<Skeleton> skeleton = skeletons.iterator(); skeleton.hasNext();) {
				if(player.collides(skeleton.next()) != null) {
					if(player.attacking) {
						skeleton.remove();
						points += 500; 
						killCount++;
					} else {
						player.die();
						break;
					}
				}
			} 
			for (Iterator<EndPoint> end = endFlags.iterator(); end.hasNext();) {
				if(player.collides(end.next()) != null) {
					//					Main.game.enterState(Main.gameState.LEVELOVER.value);
					if(currentLevel < levelCount) {
						levelUp();
					} else { 
						GameOver.win = true;
						gameOver(Color.green);
					}

					// If there is another level, go to the inBetween state (stil needs to be made)
					// If this is the last level, go to the GameOver state, but display a winning message instead of the losing one
				}
			}

			player.update(delta);
			for(Ghost enemy : ghosts)
				enemy.update(delta);
			for(Skeleton enemy : skeletons) 
				enemy.update(delta);
			for(Collectible grab : collectibles)
				grab.update();
			for(EndPoint end : endFlags)
				end.update();
		}
	}

	public static void pause() {
		if(paused) {
			paused = false;
		} else {
			paused = true;
		}
	}

	static int getBlockID(float x, float y, int layerNum) {
		int xBlock = (int)x / GameState.tileSize;
		int yBlock = (int)y / GameState.tileSize;

		if(layerNum == layer.spawn.value) {
			return spawnLayer[xBlock][yBlock];
		} else if(layerNum == layer.collision.value) {
			return blockedLayer[xBlock][yBlock];
		} else {
			return backgroundLayer[xBlock][yBlock];
		}

	}

	public int getID() {
		return Main.gameState.GAMESTATE.value;
	}

	@Override
	protected void enterState(GameContainer container, StateBasedGame game)
			throws SlickException {
	}

	@Override
	protected void leaveState(GameContainer container, StateBasedGame game)
			throws SlickException {
	}

	@Override
	protected void prepareNifty(Nifty nifty, StateBasedGame game) {
		PC = new PauseController();
		nifty.fromXml("niftyRes/pause.xml", "start", PC);
	}

}