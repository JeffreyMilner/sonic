package sonic;

import org.newdawn.slick.Color;
import org.newdawn.slick.state.transition.RotateTransition;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.NiftyEventSubscriber;
import de.lessvoid.nifty.elements.events.NiftyMousePrimaryClickedEvent;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.screen.ScreenController;

public class ControlController implements ScreenController {
	public void bind(Nifty arg0, Screen arg1) { } 
	public void onEndScreen() { }
	public void onStartScreen() { }
	
	@NiftyEventSubscriber(id="btn-control-back")
	public void onClick(String id, NiftyMousePrimaryClickedEvent event ){
		Main.game.enterState(Main.gameState.MAINMENU.value, null, new RotateTransition(Color.black));
	}
}
