package sonic;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.NiftyEventSubscriber;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.elements.events.NiftyMousePrimaryClickedEvent;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.screen.ScreenController;

public class PauseController implements ScreenController {
	public Nifty nifty;
	public Screen screen;
	Element popupMenu;

	public void bind(Nifty nifty, Screen screen) {
		this.nifty = nifty;
		this.screen = screen;
		System.out.print("Pop Up Exit");

		popupMenu = this.nifty.createPopup("PopupExitID");
	}

	public void onStartScreen() { } 
	public void onEndScreen() { }

	@NiftyEventSubscriber(pattern="Btn-.*")
	public void onClick(String id, NiftyMousePrimaryClickedEvent event ){
		if(id.equals("Btn-ResumeGame")) {
			GameState.pause();
			exitPauseMenu(); 
		} else if(id.equals("Btn-MainMenu")) {
			Main.game.enterState(Main.gameState.MAINMENU.value);
			GameState.pause();
			exitPauseMenu();
		} else if(id.equals("Btn-ExitGame")) {
			System.exit(0);
		} 
	}

	public void exitPauseMenu() { this.nifty.closePopup( popupMenu.getId()); }
	public void showPauseMenu() { this.nifty.showPopup(this.nifty.getCurrentScreen(), popupMenu.getId(), null); }
}
