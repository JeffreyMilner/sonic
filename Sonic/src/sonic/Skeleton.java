package sonic;

import org.newdawn.slick.Animation;

import jig.Entity;
import jig.ResourceManager;

public class Skeleton extends Entity {

	public final int origX;		// The origional x, so that I can keep track of where it started
	public int origOffset = 0;
	private int xVelocity = -2; 	// It's x velocity
	private int range = 0;		// How far to each side it can go

	private Animation leftMove, rightMove;

	public Skeleton(int x, int y, int dist) {
		super(x, y);
		origX = x;
		setRange(dist*32); // Gives the enemy a range of blocks it can move
		leftMove = new Animation(ResourceManager.getSpriteSheet(Main.SKELLY_ANIM, 20, 32), 0, 0, 3, 0, true, 150, true);
		rightMove = new Animation(ResourceManager.getSpriteSheet(Main.SKELLY_ANIM, 20, 32), 0, 1, 3, 1, true, 150, true);
		addImageWithBoundingBox(ResourceManager.getImage(Main.ENEMY_IMG)); 	// Just adding this because I was not sure how to get a bounding box on the animation
		addAnimation(leftMove);
	}

	public void update(int delta) {
		origOffset += xVelocity;
		setX((origX + GameState.xOffset + origOffset));
			
		if(Math.abs(origOffset) > range) {
//			System.out.println("min: " + (origX - range) + ", x: " + (getX()-Level.xOffset) + ", max: " + (origX + range) + ", range: " + range + ", xOffset: " + Level.xOffset + ", origOffset: " + origOffset);
			xVelocity *= -1;
			if(xVelocity >= 0) {
				removeAnimation(leftMove);
				addAnimation(rightMove);
				rightMove.setLooping(true);
			} else {
				removeAnimation(rightMove);
				addAnimation(leftMove);
				leftMove.setLooping(true);
			}
		}
	}

	public int getxVelocity() { return xVelocity; }
	public void setxVelocity(int xVelocity) { this.xVelocity = xVelocity; }

	public int getRange() { return range; } 
	public void setRange(int range) { this.range = range; } 
}
