package sonic;

import java.awt.Font;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.state.StateBasedGame;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.slick2d.NiftyOverlayGameState;


public class LevelOver extends NiftyOverlayGameState {

	TrueTypeFont font;

	public LevelOver(int state) { }

	protected void initGameAndGUI(GameContainer container, StateBasedGame game) throws SlickException {
		initNifty(container, game);

		Font awtFont4 = new Font("Calibre", Font.BOLD, 20);
		font = new TrueTypeFont(awtFont4, false);
	}

	public int middleOf(String text) {
		return Main.screenWidth/2 - font.getWidth(text)/2;
	}

	protected void enterState(GameContainer container, StateBasedGame game) throws SlickException { }
	protected void leaveState(GameContainer container, StateBasedGame game) throws SlickException { }
	protected void renderGame(GameContainer container, StateBasedGame game, Graphics g) throws SlickException {
		String Congrats 	= "Congratulations, you have beat level " + (GameState.currentLevel - 1) + " out of " + GameState.levelCount;
//		String Nothing1 	= "You seem to have killed no enemies and collected no items...";
//		String Nothing2 	= "While this is quite impressive, it does not get you very far";
//		String Nothing3 	= "You will probably not last long, and will have no supplies to sustain yourself.";
//		String Some1 		= "You have collected only " + GameState.grabCount + " of the items,";
//		String Some2 		= "and killed only " + GameState.killCount + " of the enemies, decent job";
//		String justEnemies1 = "You have only killed enemies and collected no items, ";
//		String justEnemies2 = "you will not be killed in the night";
//		String justItems1 	= "You have only collected items and have killed no enemies, ";
//		String justItems2	= "you will most likely be killed in the night";
//		String All1 		= "Excellent job, you have collected all " + GameState.itemTotal + ", ";
//		String All2 		= "and killed all " + GameState.enemyTotal + " enemies!";
		
		String Stats1 = "You have eliminated " + GameState.killCount + " out of " + GameState.enemyTotal + " enemies,";
		String Stats2 = "And " + GameState.grabCount + " out of " + GameState.itemTotal + " items";

		int ix = 2;
		font.drawString(middleOf(Congrats), ix++ * 30, Congrats, Color.blue);
//		if(GameState.grabCount == 0 && GameState.killCount == 0) {
//			font.drawString(middleOf(Nothing1), ix++ * 30, Nothing1, Color.green);
//			font.drawString(middleOf(Nothing2), ix++ * 30, Nothing2, Color.green);
//			font.drawString(middleOf(Nothing3), ix++ * 30, Nothing3, Color.green);
//		} else if(GameState.grabCount == 0) {
//			font.drawString(middleOf(justEnemies1), ix++ * 30, justEnemies1, Color.green);
//			font.drawString(middleOf(justEnemies2), ix++ * 30, justEnemies2, Color.green);
//		} else if(GameState.killCount == 0) {
//			font.drawString(middleOf(justItems1), ix++ * 30, justItems1, Color.green);
//			font.drawString(middleOf(justItems2), ix++ * 30, justItems2, Color.green);
//		} else if(GameState.grabCount == GameState.itemTotal && GameState.killCount == GameState.enemyTotal) { 
//			font.drawString(middleOf(All1), ix++ * 30, All1, Color.green);
//			font.drawString(middleOf(All2), ix++ * 30, All2, Color.green);
//		} else if(GameState.grabCount > 0 && GameState.killCount > 0) {
//			font.drawString(middleOf(Some1), ix++ * 30, Some1, Color.green);
//			font.drawString(middleOf(Some2), ix++ * 30, Some2, Color.green);
//		}
		font.drawString(middleOf(Stats1), ix++ * 30, Stats1, Color.green);
		font.drawString(middleOf(Stats2), ix++ * 30, Stats2, Color.green);

	}
	protected void updateGame(GameContainer container, StateBasedGame game, int delta) throws SlickException { }

	protected void prepareNifty(Nifty nifty, StateBasedGame game) { 
		LevelOverController LOC = new LevelOverController();

		nifty.fromXml("niftyRes/levelOver.xml", "levelOver", LOC);
	}

	public int getID() {
		return Main.gameState.LEVELOVER.value;
	}

	public void mouseWheelMoved(int change) { }
	public void mouseClicked(int button, int x, int y, int clickCount) { }
	public void mousePressed(int button, int x, int y) { }
	public void mouseReleased(int button, int x, int y) { }
	public void mouseMoved(int oldx, int oldy, int newx, int newy) { }
	public void mouseDragged(int oldx, int oldy, int newx, int newy) { }
	public void setInput(Input input) { }
	public boolean isAcceptingInput() { return false; }
	public void inputEnded() { }
	public void inputStarted() { }
	public void keyPressed(int key, char c) { }
	public void keyReleased(int key, char c) { }
	public void controllerLeftPressed(int controller) { }
	public void controllerLeftReleased(int controller) { }
	public void controllerRightPressed(int controller) { }
	public void controllerRightReleased(int controller) { }
	public void controllerUpPressed(int controller) { }
	public void controllerUpReleased(int controller) { }
	public void controllerDownPressed(int controller) { }
	public void controllerDownReleased(int controller) { }
	public void controllerButtonPressed(int controller, int button) { }
	public void controllerButtonReleased(int controller, int button) { }
}
