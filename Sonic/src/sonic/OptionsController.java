package sonic;

import org.newdawn.slick.gui.ComponentListener;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.NiftyEventSubscriber;
import de.lessvoid.nifty.controls.RadioButtonGroupStateChangedEvent;
import de.lessvoid.nifty.elements.events.NiftyMousePrimaryClickedEvent;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.screen.ScreenController;

public class OptionsController implements ScreenController {

	private ComponentListener acceptListener;

	public static boolean wasd = false;		// Move with the A and D keys, maybe W for jump if you have spaceJump as false
	public static boolean spaceJump = true;	// Jump with the spacebar

	/**
	 * This is called when the RadioButton selection has changed.
	 */
	@NiftyEventSubscriber(pattern="RadioGroup-.*")
	public void onRadioButtonGroupChanged(final String id, final RadioButtonGroupStateChangedEvent event) {
		if(id.equals("RadioGroup-Move")) {
			if(event.getSelectedId().equals("OptionMenu-WASD")) {
				wasd = true;
			} else if(event.getSelectedId().equals("OptionMenu-Arrow")) {
				wasd = false;
			}
		} else if(id.equals("RadioGroup-Jump")) {
			if(event.getSelectedId().equals("OptionMenu-Space")) {
				spaceJump = true;
			} else if(event.getSelectedId().equals("OptionMenu-Up")) {
				spaceJump = false;
			}
		}
	}

	@NiftyEventSubscriber(id="btn-Option-Accept")
	public void onClick(String id, NiftyMousePrimaryClickedEvent event ){
		if(acceptListener != null) {
			acceptListener.componentActivated(null);
		}
	}

	public void onAcceptPress(ComponentListener press) {
		acceptListener = press;
	}

	@Override
	public void bind(Nifty arg0, Screen arg1) { } 
	@Override
	public void onEndScreen() { } 
	@Override
	public void onStartScreen() { }
}