package sonic;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.gui.AbstractComponent;
import org.newdawn.slick.gui.ComponentListener;
import org.newdawn.slick.state.StateBasedGame;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.slick2d.NiftyOverlayGameState;

// wasd vs arrows
public class Options extends NiftyOverlayGameState {

	public Options(int value) {
	}

	@Override
	protected void initGameAndGUI(GameContainer container, StateBasedGame game) throws SlickException {
		initNifty(container, game);
	}

	@Override
	protected void prepareNifty(Nifty nifty, StateBasedGame game) {
		final OptionsController optC = new OptionsController();
		
		nifty.fromXml("niftyRes/options.xml", "options", optC);
		
		
		optC.onAcceptPress(new ComponentListener(){
			public void componentActivated( AbstractComponent arg0 ) {
				Main.game.enterState(Main.gameState.MAINMENU.value);
			}
		});
	}
	
	@Override
	public int getID() {
		return Main.gameState.OPTIONS.value;
	}
	
	@Override
	public void mouseWheelMoved(int change) {
	}
	@Override
	public void mouseClicked(int button, int x, int y, int clickCount) {
	}
	@Override
	public void mousePressed(int button, int x, int y) {
	}
	@Override
	public void mouseReleased(int button, int x, int y) {
	}
	@Override
	public void mouseMoved(int oldx, int oldy, int newx, int newy) {
	}
	@Override
	public void mouseDragged(int oldx, int oldy, int newx, int newy) {
	}
	@Override
	public void setInput(Input input) {
	}
	@Override
	public boolean isAcceptingInput() {
		return false;
	}
	@Override
	public void inputEnded() {
	}
	@Override
	public void inputStarted() {
	}
	@Override
	public void keyPressed(int key, char c) {
	}
	@Override
	public void keyReleased(int key, char c) {
	}
	@Override
	public void controllerLeftPressed(int controller) {
	}
	@Override
	public void controllerLeftReleased(int controller) {
	}
	@Override
	public void controllerRightPressed(int controller) {
	}
	@Override
	public void controllerRightReleased(int controller) {
	}
	@Override
	public void controllerUpPressed(int controller) {
	}
	@Override
	public void controllerUpReleased(int controller) {
	}
	@Override
	public void controllerDownPressed(int controller) {
	}
	@Override
	public void controllerDownReleased(int controller) {
	}
	@Override
	public void controllerButtonPressed(int controller, int button) {
	}
	@Override
	public void controllerButtonReleased(int controller, int button) {
	}
	@Override
	protected void enterState(GameContainer arg0, StateBasedGame arg1) throws SlickException {
	}
	@Override
	protected void leaveState(GameContainer arg0, StateBasedGame arg1) throws SlickException {
	}
	@Override
	protected void renderGame(GameContainer arg0, StateBasedGame arg1, Graphics arg2) throws SlickException {
	}
	@Override
	protected void updateGame(GameContainer arg0, StateBasedGame arg1, int arg2) throws SlickException {
	}
}
