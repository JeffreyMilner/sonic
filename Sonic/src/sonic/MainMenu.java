package sonic;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.gui.AbstractComponent;
import org.newdawn.slick.gui.ComponentListener;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.state.transition.BlobbyTransition;
import org.newdawn.slick.state.transition.FadeInTransition;
import org.newdawn.slick.state.transition.FadeOutTransition;
import org.newdawn.slick.state.transition.RotateTransition;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.slick2d.NiftyOverlayGameState;


public class MainMenu extends NiftyOverlayGameState {

//	public static boolean oldWasd = false;
//	public static boolean oldJump = false;
	
	public MainMenu(int state) {

	}

	@Override
	protected void initGameAndGUI(GameContainer container, StateBasedGame game) throws SlickException {
		initNifty(container, game);
	}
	
	@Override
	protected void prepareNifty(Nifty nifty, StateBasedGame game) { 
		MainMenuController MMC = new MainMenuController();

		nifty.fromXml("niftyRes/mainMenu.xml", "mainMenu", MMC);

		MMC.onStartPress(new ComponentListener(){
			@Override
			public void componentActivated( AbstractComponent arg0 ) {
				GameState.resetGame();
				Main.game.enterState(Main.gameState.GAMESTATE.value, new FadeOutTransition(Color.green, 1000), new FadeInTransition(Color.green, 1000));
			}
		});
//		MMC.onOptionsPress(new ComponentListener(){
//			public void componentActivated( AbstractComponent arg0 ) {
//				oldJump = OptionsController.spaceJump;
//				oldWasd = OptionsController.wasd;
//				Main.game.enterState(Main.gameState.OPTIONS.value);
//			}
//		});
		MMC.onControlsPress(new ComponentListener(){
			public void componentActivated( AbstractComponent arg0 ) {
				Main.game.enterState(Main.gameState.CONTROLS.value, null, new BlobbyTransition(Color.black));			
			}
		});
		MMC.onCreditsPress(new ComponentListener(){
			public void componentActivated( AbstractComponent arg0 ) {
				Main.game.enterState(Main.gameState.CREDITS.value, null, new RotateTransition(Color.green));			
			}
		});
		MMC.onExitPress(new ComponentListener(){
			public void componentActivated( AbstractComponent arg0 ) {
				System.exit(0);
			}
		});
		
	}
	
	protected void enterState(GameContainer container, StateBasedGame game) throws SlickException { }
	protected void leaveState(GameContainer container, StateBasedGame game) throws SlickException { }
	protected void renderGame(GameContainer container, StateBasedGame game, Graphics g) throws SlickException { }
	protected void updateGame(GameContainer container, StateBasedGame game, int delta) throws SlickException { }
	
	public int getID() {
		return Main.gameState.MAINMENU.value;
	}
	
	public void mouseWheelMoved(int change) { }
	public void mouseClicked(int button, int x, int y, int clickCount) { }
	public void mousePressed(int button, int x, int y) { }
	public void mouseReleased(int button, int x, int y) { }
	public void mouseMoved(int oldx, int oldy, int newx, int newy) { }
	public void mouseDragged(int oldx, int oldy, int newx, int newy) { }
	public void setInput(Input input) { }
	public boolean isAcceptingInput() { return false; }
	public void inputEnded() { }
	public void inputStarted() { }
	public void keyPressed(int key, char c) { }
	public void keyReleased(int key, char c) { }
	public void controllerLeftPressed(int controller) { }
	public void controllerLeftReleased(int controller) { }
	public void controllerRightPressed(int controller) { }
	public void controllerRightReleased(int controller) { }
	public void controllerUpPressed(int controller) { }
	public void controllerUpReleased(int controller) { }
	public void controllerDownPressed(int controller) { }
	public void controllerDownReleased(int controller) { }
	public void controllerButtonPressed(int controller, int button) { }
	public void controllerButtonReleased(int controller, int button) { }
}
